import { Injectable } from '@angular/core';
import { Log } from '../models/log';

@Injectable()
export class LogService {

  logs: Log[];

  constructor() {
    this.logs = [
      { id: '1', text: 'Generated  componenets', date: new Date('12/26/2017 12:54:23') },
      { id: '1', text: 'Added Bootsrap', date: new Date('12/28/2017 12:54:23') },
      { id: '1', text: 'Added logs Componenents', date: new Date('12/26/2017 12:54:23') }

    ];
  }

  getLogs() {
    return this.logs;
  }

}
